# Museum phylogenomics of extinct *Oryctes* beetles from the Mascarene Islands
Preprint doi: https://doi.org/10.1101/2020.02.19.954339

1. Genotypes (.map and .ped) files have been produced with [bsh-denovo](https://github.com/clwgg/bsh-denovo)
```
bsh-denovo -m 1 -a 0.001 -f 0.28 -o dataset.maf0.28.0.001.biall.fullinfo multi.bam
```


2. [Plink](https://www.cog-genomics.org/plink/) has been used to create transposed (.tped and .tfam) files.


3. The script tped2fasta.sh was used to create a pseudo fasta file to do the ML phylogeny, Bayesian phylogeny and Network analyses.
```
bash tped2fasta.sh <dataset.maf0.28.0.001.biall.fullinfo.tped>
```


4. The script calculate_windowed_divergence_from_bam.py was used to calculate nucleotide divergence between a given sample (.bam) and a reference.
```
python3 calculate_windowed_divergence_from_bam.py <beetle_reference.fasta> <sample.bam>
```

