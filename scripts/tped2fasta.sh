# Given a .tped file, this script returns a pseudo fasta file. The order and names of the samples will be those of the correspondent .tfam file
# USAGE: bash tped2fasta.sh <file.tped>

tped=$1
tfam=$(echo $tped | sed 's/tped/tfam/g')

nmax=$(sed -n 1p $tped | awk '{print NF - 1}')

for i in $(seq 5 2 $nmax); do cut -d " " -f $i $tped | tr "\n" "X" | sed 's/X//g' >> $tped.tmp && echo "" >> $tped.tmp ; done
cut -d " " -f1 $tfam | sed 's/^/>/g' > $tped.names.tmp
paste -d "\n" $tped.names.tmp $tped.tmp

rm $tped.names.tmp $tped.tmp
