# Given a reference genome in .fasta format and a sorted .bam file with the same coordinate system, this script produces a summary of genetic distances per window.
# Thw window size and the minumum data per window are hardcoded. Change those values carefuly.
# USAGE: python3 calculate_windowed_divergence_from_bam.py <reference.fasta> <mappedreads.bam>
#
# Output:
# CONTIG	WINDOW_START	WINDOW_END	DIFFERENT	TOTAL	DIFFERENT/TOTAL

import pysam
from Bio import SeqIO
from random import sample
from sys import argv

reference = argv[1]
bam = pysam.AlignmentFile(argv[2], 'rb')

wsize = 100000
data_per_window = 50
all_pos = False # Switch here to print all positions. Positions with no data will be NAs

for contig in SeqIO.parse(reference, 'fasta'):
    ref_seq = contig.seq
    for window in range(0, len(ref_seq), wsize):
        total = 0
        different = 0
        for pile in bam.pileup(contig.id, window, window+wsize):
            if pile.n >= 1 and ref_seq[pile.pos] != 'N': # At least 1X
                bases = []
                for pileupread in pile.pileups:
                    if not pileupread.is_del and not pileupread.is_refskip: # Just SNPs
                        bases.append(pileupread.alignment.query_sequence[pileupread.query_position])
                if len(bases) >= 1: # At least 1X
                    if ref_seq[pile.pos] != sample(bases, 1)[0]:
                        total += 1
                        different += 1
                    else:
                        total += 1
        if total >= data_per_window:
            print(contig.id, window, window+wsize, different, total, different/total, sep='\t', flush=True)
        else:
            if all_pos == True:
                print(contig.id, window, window+wsize, different, total, "NA", sep='\t', flush=True)
