# DESCRIPTION: Given a four taxa genealogy, this script counts the number and proportion of Concordant and Discordant genealogies. Output will be given as standart output.
# Concordant: BBAA
# Discordant: ABBA, BABA
# Requirements: Transposed ped and fam files. Both tped and tfam files must have the same prefix name. example_name.tped, example_name.tfam
# USAGE: python3 count_sites.py -i <files_without_extension>
# -i Interactive mode: Reads the names and user must select the correct genealogy
# Or, without interactive mode:
# python3 count_sites.py Taxa1,Taxa2,Taxa3,Outgroup <files_without_extension>

from sys import argv

tped = argv[-1] + '.tped'
tfam = argv[-1] + '.tfam'

with open(tfam, 'r') as f:
    names = [line.split(' ')[0] for line in f]

tree = []
if argv[1] == '-i':
    print('Given the following topology:')
    print('   /\\')
    print('  /\\ \\')
    print(' /\\ \\ \\')
    print('A B C O')
    print('\nand the following taxa names:')
    for i in range(len(names)):
        print('{}: {}'.format(i, names[i]))
    print('')
    pos = ('A', 'B', 'C', 'O')
    for i in range(4):
        tree.append(int(input('Select the taxa for the tip {}: '.format(pos[i]))))
else:
    for i in range(4):
        tree.append(names.index(argv[1].split(',')[i]))
    print('')
#concordant = ('AAAA', 'BBAA', 'ABAA', 'BAAA', 'BBBA', 'AABA')
concordant = ['BBAA']
discordant = ['BABA', 'ABBA']

counts = {i:0 for i in (concordant + discordant)}
with open(tped, 'r') as f:
    for line in f:
        genot = [line.split(' ')[((i + 1) * 2) +2 ] for i in tree]
        if 'N' in genot or '0' in genot:
        #if 'N' in genot or '0' in genot or genot.count(genot[0]) == 4:
            continue
        site = ''
        for i in range(3):
            if genot[i] == genot[3]:
                site = site + 'A'
            else:
                site = site + 'B'
        if site + 'A' in concordant + discordant:
            counts[site + 'A'] += 1

print('# ((({}, {}), {}), {})'.format(names[tree[0]], names[tree[1]], names[tree[2]], names[tree[3]]))

for i in counts:
    print(i, counts[i], round(counts[i] / sum(counts.values()), 3), sep = '\t')
print('Concordant', sum([counts[i] for i in concordant]), round(sum([counts[i] for i in concordant]) / sum(counts.values()), 3), sep = '\t')
print('Discordant', sum([counts[i] for i in discordant]), round(sum([counts[i] for i in discordant]) / sum(counts.values()), 3), sep= '\t')